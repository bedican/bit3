# Bit3

Bit3 is a simple tool to download a BitBucket repository and upload to an AWS S3 bucket.

The main purpose of this tool is to work around a missing feature and allow BitBucket to be used as a source with AWS CodePipeline. Currently BitBucket cannot be used as a CodePipeline source, so alternatively, we can use S3 instead, and push a repository contents to S3 which can trigger the pipeline.

## Installation

``` bash
# Install the cli tool
$ npm install -g bit3
```

## Usage

``` bash
# Deploy the default application (see configuration below)
$ bit3 default
```

## Configuration

The configuration file is located at `~/.config/bit3.json` and consists of the following options.

```json
{
        "default": {
                "repositoryAccount": "<user or team>",
                "repositoryName": "<repository name>",
                "repositoryBranch": "<repository branch>",
                "appUsername": "<login username>",
                "appPassword": "<app password>",
                "awsProfile": "<aws credentials file profile>",
                "s3Bucket": "<bucket>",
                "s3Key": "<bucket key>"
        }
}
```

### Options

| Option | Description |
| -- | -- |
| repositoryAccount | The BitBucket username or team that the repository resides under. |
| repositoryName | The BitBucket repository name. |
| repositoryBranch | The BitBucket repository branch. Defaults to `master`. |
| appUsername | The username of the BitBucket account, (not the Atlassian login email). This can be found within BitBucket profile settings. |
| appPassword | An App password. Created within the BitBucket profile settings. |
| awsProfile | The profile to use within the aws credtials file, `~/.aws/credentials`. Defaults to `bit3`. |
| s3Bucket | The S3 bucket to upload to. |
| s3Key | The key of the S3 bucket for the uploaded zip file. |

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
