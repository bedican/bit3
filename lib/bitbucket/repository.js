const request = require('request');
const fs = require('fs');

class BitbucketRepository {

    constructor(account, name) {
        this.account = account;
        this.name = name;
    }

    async download(filename, branch, username, password) {
        return new Promise((resolve, reject) => {
            request('https://bitbucket.org/' + this.account + '/' + this.name + '/get/' + branch + '.zip')
                .auth(username, password)
                .pipe(fs.createWriteStream(filename))
                .on('finish', () => {
                    resolve();
                })
                .on('error', (err) => {
                    reject(err);
                });
        });
    }
}

module.exports = BitbucketRepository;
