const tmp = require('tmp');
const util = require('util');
const rimraf = util.promisify(require('rimraf'));
const randomstring = require("randomstring");

class TmpDir {
    constructor() {
        this.tmpDir = tmp.dirSync();
    }

    generateFilename() {
        return this.tmpDir.name + '/' + randomstring.generate(12);
    }

    async cleanup() {
        await rimraf(this.tmpDir.name);
    }
}

module.exports = TmpDir;
