const AdmZip = require('adm-zip');
const stripDirs = require('strip-dirs');

class ZipFile {

    constructor(filename) {
        this.zip = new AdmZip(filename);
    }

    stripComponents(level, filename) {
        let _this = this;

        let newZip = new AdmZip();

        this.zip.getEntries().forEach(function(entry) {
            if (entry.isDirectory) {
                return;
            }

            let data = _this.zip.readFile(entry);
            let entryName = stripDirs(entry.entryName, level);
            newZip.addFile(entryName, data);
        });

        newZip.writeZip(filename);
    }
}

module.exports = ZipFile;
