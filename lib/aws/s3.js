const fs = require('fs');

class S3  {
    constructor(awsS3) {
        this.awsS3 = awsS3;
    }

    async put(filename, bucket, key) {
        let body = fs.createReadStream(filename);

        return this.awsS3.putObject({
            Bucket: bucket,
            Key: key,
            Body: body
        }).promise();
    }
}

module.exports = S3;
