const AWS = require('aws-sdk');
const S3 = require('./s3');

class Aws {
    constructor(profile) {
        AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: profile });
    }

    s3() {
        return new S3(new AWS.S3());
    }
}

module.exports = Aws;
