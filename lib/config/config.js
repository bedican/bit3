const os = require('os');
const fs = require('fs');

class Config {
    constructor(filename) {
        try {
            this.config = JSON.parse(fs.readFileSync(os.homedir() + '/.config/' + filename, 'utf8'));
        } catch (e) {
            throw new Error('Failed to read configuration');
        }
    }

    getConfig(name) {
        if (!this.config[name]) {
            throw new Error('Unknown config: ' + name);
        }

        return this.config[name];
    }
}

module.exports = Config;
