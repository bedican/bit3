const Config = require('./config/config');
const TmpDir = require('./fs/tmp-dir');
const BitbucketRepository = require('./bitbucket/repository');
const ZipFile = require('./zip/zip-file');
const Aws = require('./aws/aws');

module.exports = async (appName) => {
    try {

        let config = new Config('bit3.json');
        let appConfig = config.getConfig(appName);

        let tmpDir = new TmpDir();
        let zipFileFull = tmpDir.generateFilename();
        let zipFileStripped = tmpDir.generateFilename();

        let repository = new BitbucketRepository(appConfig.repositoryAccount, appConfig.repositoryName);
        await repository.download(zipFileFull, appConfig.repositoryBranch || 'master', appConfig.appUsername, appConfig.appPassword);

        let zip = new ZipFile(zipFileFull);
        zip.stripComponents(1, zipFileStripped);

        let aws = new Aws(appConfig.awsProfile || 'bit3');
        let s3 = aws.s3();

        await s3.put(zipFileStripped, appConfig.s3Bucket, appConfig.s3Key);

        tmpDir.cleanup();

    } catch (err) {
        console.error(err);
    }
};
